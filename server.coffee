express = require("express")
http = require("http")
sysPath = require("path")
slashes = require("connect-slashes")

startServer = (port,publicPath, callback) ->
  options = {
    path: publicPath,
    port: 8083,
    base: ''
  }
  options.indexPath = sysPath.join(options.path, "index.html")  unless options.indexPath?
  options.noCors = false  unless options.noCors?
  options.stripSlashes = false  unless options.stripSlashes?
  options.noPushState = false  unless options.noPushState?
  options.noLog = false  unless options.noLog?
  callback = (->) unless callback?
  app = express()

  # Send cross-origin resource sharing enabling header.
  unless options.noCors
    app.use (request, response, next) ->
      response.header "Cache-Control", "no-cache"
      response.header "Access-Control-Allow-Origin", "*"
      next()


  # Route all static files to http paths.
  app.use options.base, express.static(sysPath.resolve(options.path))

  # Redirect requests that include a trailing slash.
  app.use slashes(false)  if options.stripSlashes

  # Route all non-existent files to `index.html`
  unless options.noPushState
    app.all "" + options.base + "/*", (request, response) ->
      response.sendfile options.indexPath


  # Wrap express app with node.js server in order to have stuff like server.stop() etc.
  server = http.createServer(app)
  server.listen options.port, (error) ->
    console.log "Application started on http://localhost:" + options.port  unless options.noLog
    callback error, options

  server

module.exports.startServer = startServer