path = (paths) ->
  # (\\/|\\\\) is (\|/), windows or unix-style directory separators
  paths.join && paths = paths.join('|')
  paths = paths.replace(/\//g, '(\\/|\\\\)')
  new RegExp('^' + paths)

exports.config =
  plugins:
    react:
      autoIncludeCommentBlock: yes
      harmony: yes
  conventions:
    assets: path('app/assets')
  modules2:
    definition: false
    wrapper: false
  paths:
    public: 'public'
  vendor: [
      'fonts'
    ]
  coffeelint:
    pattern: /^app\/.*\.coffee$/
    options:
      indentation:
        value: 2
        level: "error"
      max_line_length:
        value: 80
        level: "ignore"
  files:
    javascripts:
      joinTo:
        'javascripts/app.js': /^app/
        'javascripts/vendor.js': /^vendor|bower_components/
      order:
        before: [
          "bower_components/react/react-with-addons.js",
          "bower_components/react-async/react-async.js",
          "bower_components/react-router-component-bower/react-router-component.js"
        ]
    stylesheets:
      defaultExtension: 'styl'
      joinTo: 'css/app.css'
      order:
        before: [
          'app/css/*'
        ]
        after: [

        ]
  server:
    port: 3334
    path: 'server.coffee'
    publicPath: '../hookworm/static'
    #Run even without `--server` option?
    run: yes
  plugins:
    coffeelint:
      pattern: /^app\/.*\.coffee$/
      options: require('./coffeelint.json')
  watcher:
    usePolling: true

