Router = ReactRouter
RouteHandler = Router.RouteHandler
DefaultRoute = Router.DefaultRoute
Link = Router.Link
Route = Router.Route
RouteHandler = Router.RouteHandler

App = React.createClass(
  componentDidMount: ->
    node = @getDOMNode()
    TweenMax.to(node, 1, {opacity:1})
  render: ->
    <div className="app">
      <header>
        <ul>
          <li><Link to="app">Dashboard</Link></li>
          <li><Link to="inbox">Inbox</Link></li>
          <li><Link to="calendar">Calendar</Link></li>
        </ul>
      </header>
      <RouteHandler/>
    </div>
)

Inbox = React.createClass(
  render: ->
    <div>
    <Toolbar/>
    <RouteHandler/>
    </div>
)
Calendar = React.createClass(
  render: ->
    <div className="calendar">
    <h1>Calendar</h1>
    </div>

)
Dashboard = React.createClass(
  render: ->
      <div className="dashboard">
      <h1>Dashboard</h1>
      </div>
)
Toolbar = React.createClass(
  render: ->
      <div>
        <h1>Toolbar</h1>
      </div>
)

routes = (
  <Route name="app" path="/" handler={App}>
    <Route name="inbox" handler={Inbox}/>
    <Route name="calendar" handler={Calendar}/>
    <DefaultRoute handler={Dashboard}/>
  </Route>
)
Router.run(routes, Router.HistoryLocation, (Handler) ->
  React.render(<Handler/>, document.body);
)