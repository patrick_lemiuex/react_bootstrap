(function() {
  'use strict';

  var globals = typeof window === 'undefined' ? global : window;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};
  var has = ({}).hasOwnProperty;

  var aliases = {};

  var endsWith = function(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  };

  var unalias = function(alias, loaderPath) {
    var start = 0;
    if (loaderPath) {
      if (loaderPath.indexOf('components/' === 0)) {
        start = 'components/'.length;
      }
      if (loaderPath.indexOf('/', start) > 0) {
        loaderPath = loaderPath.substring(start, loaderPath.indexOf('/', start));
      }
    }
    var result = aliases[alias + '/index.js'] || aliases[loaderPath + '/deps/' + alias + '/index.js'];
    if (result) {
      return 'components/' + result.substring(0, result.length - '.js'.length);
    }
    return alias;
  };

  var expand = (function() {
    var reg = /^\.\.?(\/|$)/;
    return function(root, name) {
      var results = [], parts, part;
      parts = (reg.test(name) ? root + '/' + name : name).split('/');
      for (var i = 0, length = parts.length; i < length; i++) {
        part = parts[i];
        if (part === '..') {
          results.pop();
        } else if (part !== '.' && part !== '') {
          results.push(part);
        }
      }
      return results.join('/');
    };
  })();
  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var absolute = expand(dirname(path), name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';
    path = unalias(name, loaderPath);

    if (has.call(cache, path)) return cache[path].exports;
    if (has.call(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has.call(cache, dirIndex)) return cache[dirIndex].exports;
    if (has.call(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  require.alias = function(from, to) {
    aliases[to] = from;
  };

  require.register = require.define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has.call(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  require.list = function() {
    var result = [];
    for (var item in modules) {
      if (has.call(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  require.brunch = true;
  globals.require = require;
})();
require.register("app", function(exports, require, module) {
var App, Calendar, Dashboard, DefaultRoute, Inbox, Link, Route, RouteHandler, Router, Toolbar, routes;

Router = ReactRouter;

RouteHandler = Router.RouteHandler;

DefaultRoute = Router.DefaultRoute;

Link = Router.Link;

Route = Router.Route;

RouteHandler = Router.RouteHandler;

App = React.createClass({
  type: 'class',
  render: function() {
    return React.createElement("div", null, React.createElement("header", null, React.createElement("ul", null, React.createElement("li", null, React.createElement(Link, {
      "to": "app"
    }, "Dashboard")), React.createElement("li", null, React.createElement(Link, {
      "to": "inbox"
    }, "Inbox")), React.createElement("li", null, React.createElement(Link, {
      "to": "calendar"
    }, "Calendar")))), React.createElement(RouteHandler, null));
  }
});

Inbox = React.createClass({
  render: function() {
    return React.createElement("div", null, React.createElement(Toolbar, null), React.createElement(RouteHandler, null));
  }
});

Calendar = React.createClass({
  render: function() {
    return React.createElement("div", null, "Calendar");
  }
});

Dashboard = React.createClass({
  render: function() {
    return React.createElement("div", null, "Calendar");
  }
});

Toolbar = React.createClass({
  render: function() {
    return React.createElement("div", null, "Toolbar");
  }
});

routes = React.createElement(Route, {
  "name": "app",
  "path": "/",
  "handler": App
}, React.createElement(Route, {
  "name": "inbox",
  "handler": Inbox
}), React.createElement(Route, {
  "name": "calendar",
  "handler": Calendar
}), React.createElement(DefaultRoute, {
  "handler": Dashboard
}));

routes.location = "history";

Router.run(routes, Router.HistoryLocation, function(Handler) {
  return React.render(React.createElement(Handler, null), document.body);
});
});

;
//# sourceMappingURL=app.js.map